// InverseAPI
// Developed by Sahibdeep Nann (@SahibdeepNann)
// https://www.bitbucket.org/InitializeSahib/inverseapi
#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <random>
#include <cmath>
using namespace std;
namespace InverseAPI {
	int InverseRandom::randomInteger(int minimumInteger, int maximumInteger) {
		mt19937 generateInteger(time(0));
		uniform_int_distribution<int> integerDistribution(minimumInteger, maximumInteger);
		return integerDistribution(generateInteger);
	}
	double InverseMath::inverseExponents(double baseNumber, double exponentNumber) {
		return pow(baseNumber, exponentNumber);
	}
	double InverseMath::inverseSquare(double numberToSquare) {
		return numberToSquare * numberToSquare;
	}
	void InverseSystem::runSystemCommand(const char *commandToRun) {
		system(commandToRun);
	}
	time_t InverseSystem::getSystemTime() {
		return time(0);
	}
	int InverseRandom::randomDigit() {
		mt19937 generateDigit(time(0));
		uniform_int_distribution<int> integerDistribution(0, 9);
		return integerDistribution(generateDigit);
	}
	char InverseRandom::randomLetterUppercase() {
		mt19937 generateLetterInt(time(0));
		uniform_int_distribution<int> integerDistribution(1, 26);
		int randomLetterInt = integerDistribution(generateLetterInt);
		if (randomLetterInt == 1)  {
			return 'A';
		}
		if (randomLetterInt == 2)  {
			return 'B';
		}
		if (randomLetterInt == 3)  {
			return 'C';
		}
		if (randomLetterInt == 4)  {
			return 'D';
		}
		if (randomLetterInt == 5)  {
			return 'E';
		}
		if (randomLetterInt == 6)  {
			return 'F';
		}
		if (randomLetterInt == 7)  {
			return 'G';
		}
		if (randomLetterInt == 8)  {
			return 'H';
		}
		if (randomLetterInt == 9)  {
			return 'I';
		}
		if (randomLetterInt == 10)  {
			return 'J';
		}
		if (randomLetterInt == 11)  {
			return 'K';
		}
		if (randomLetterInt == 12)  {
			return 'L';
		}
		if (randomLetterInt == 13)  {
			return 'M';
		}
		if (randomLetterInt == 14)  {
			return 'N';
		}
		if (randomLetterInt == 15)  {
			return 'O';
		}
		if (randomLetterInt == 16)  {
			return 'P';
		}
		if (randomLetterInt == 17)  {
			return 'Q';
		}
		if (randomLetterInt == 18)  {
			return 'R';
		}
		if (randomLetterInt == 19)  {
			return 'S';
		}
		if (randomLetterInt == 20)  {
			return 'T';
		}
		if (randomLetterInt == 21)  {
			return 'U';
		}
		if (randomLetterInt == 22)  {
			return 'V';
		}
		if (randomLetterInt == 23)  {
			return 'W';
		}
		if (randomLetterInt == 24)  {
			return 'X';
		}
		if (randomLetterInt == 25)  {
			return 'Y';
		}
		if (randomLetterInt == 26)  {
			return 'Z';
		}
	}
	char InverseRandom::randomLetterLowercase() {
		mt19937 generateLetterInt(time(0));
		uniform_int_distribution<int> integerDistribution(1, 26);
		int randomLetterInt = integerDistribution(generateLetterInt);
		if (randomLetterInt == 1)  {
			return 'a';
		}
		if (randomLetterInt == 2)  {
			return 'b';
		}
		if (randomLetterInt == 3)  {
			return 'c';
		}
		if (randomLetterInt == 4)  {
			return 'd';
		}
		if (randomLetterInt == 5)  {
			return 'e';
		}
		if (randomLetterInt == 6)  {
			return 'f';
		}
		if (randomLetterInt == 7)  {
			return 'g';
		}
		if (randomLetterInt == 8)  {
			return 'h';
		}
		if (randomLetterInt == 9)  {
			return 'i';
		}
		if (randomLetterInt == 10)  {
			return 'j';
		}
		if (randomLetterInt == 11)  {
			return 'k';
		}
		if (randomLetterInt == 12)  {
			return 'l';
		}
		if (randomLetterInt == 13)  {
			return 'm';
		}
		if (randomLetterInt == 14)  {
			return 'n';
		}
		if (randomLetterInt == 15)  {
			return 'o';
		}
		if (randomLetterInt == 16)  {
			return 'p';
		}
		if (randomLetterInt == 17)  {
			return 'q';
		}
		if (randomLetterInt == 18)  {
			return 'r';
		}
		if (randomLetterInt == 19)  {
			return 's';
		}
		if (randomLetterInt == 20)  {
			return 't';
		}
		if (randomLetterInt == 21)  {
			return 'u';
		}
		if (randomLetterInt == 22)  {
			return 'v';
		}
		if (randomLetterInt == 23)  {
			return 'w';
		}
		if (randomLetterInt == 24)  {
			return 'x';
		}
		if (randomLetterInt == 25)  {
			return 'y';
		}
		if (randomLetterInt == 26)  {
			return 'z';
		}
	}
}