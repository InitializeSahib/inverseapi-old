// InverseAPI
// Developed by Sahibdeep Nann (@SahibdeepNann)
// https://www.bitbucket.org/InitializeSahib/inverseapi
#ifdef INVERSEAPI_EXPORTS
#define INVERSEAPI_API __declspec(dllexport)
#else
#define INVERSEAPI_API __declspec(dllimport)
#endif
#include <ctime>
using namespace std;
namespace InverseAPI {
	class InverseMath {
	public:
		static INVERSEAPI_API double inverseExponents(double baseNumber, double exponentNumber);
		static INVERSEAPI_API double inverseSquare(double numberToSquare);
	};
	class InverseSystem {
	public:
		static INVERSEAPI_API void runSystemCommand(const char *commandToRun);
		static INVERSEAPI_API time_t getSystemTime();
	};
	class InverseRandom {
	public:
		static INVERSEAPI_API int randomInteger(int minimumInteger, int maximumInteger);
		static INVERSEAPI_API int randomDigit();
		static INVERSEAPI_API char randomLetterUppercase();
		static INVERSEAPI_API char randomLetterLowercase();
	};
}