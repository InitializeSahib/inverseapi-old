**Hi! I am Sahibdeep Nann, the original creator of InverseAPI.**
**Why I forked this repo from YoshiGenius (Nick) is that he forked my old Inverse repo, and I deleted my repo. Thus I forked his repo. Please note that this will not be updated. I have moved it to GitHub. https://www.github.com/InitializeSahib/InverseAPI**

InverseAPI Readme
------------------
Contents:

1. What is InverseAPI?

2. How can I make my application use InverseAPI?

3. What does InverseAPI do?

4. How do I get the src files?

------------------
What is InverseAPI?
InverseAPI is a dynamic link library created by Sahibdeep Nann. It creates functions so C++ developers can use these functions without
hassle. It is also a required library for lots of my programs.
-------------------
How can I make my application use InverseAPI?
You will need Visual Studio 2013 or higher.
Open up the .sln file in src/InverseSolution with Visual Studio.
Then follow this guide (Find the section that shows you how to create a app that requires a .dll file):
https://msdn.microsoft.com/en-us/library/ms235636.aspx
--------------------
What does InverseAPI do?
It creates functions to make advanced tasks in C++ easier.
You can view all the functions in the source code. (src/InverseSolution/InverseAPI/InverseAPI.cpp or InverseAPI.h)
--------------------
How do I get the src files?
In the Downloads tab, there is a Download Repository link. Click that. :P